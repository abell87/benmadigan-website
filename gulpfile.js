const gulp = require('gulp');
const { series, task, src, dest } = require('gulp');
const htmlPartial = require('gulp-html-partial');

const defaults = {
  basePath: '',
  tagName: 'partial',
  variablePrefix: '@@',
};


gulp.task('html', function(cb) {
  return gulp.src(['src/*.html'])
    .pipe(htmlPartial({
      basePath: 'src/partials/'
    }))
    .pipe(gulp.dest('build'))
});

// const build = task('html');
// exports.default = series(build);
gulp.task('build', ['html']);